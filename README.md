### Hello, there! :mage:

My name's Erik graduated System Analist and Developer and front end dev at [PurpleCats](https://purplecats.tech/)

Here on Github you can check out on a few personal projects of mine.

My skills are summarized in:

- I'm more comfortable with javascript/typescript (Node and React included) 🏆
- I also work with PHP/Laravel and Java/Spring
- Know a few things about pentest 💻
- Tried some gamedev 🎮


📫 You can reach me on my personal [Linkedin profile](https://www.linkedin.com/in/erik-natan-moreira-santos-983865195/)

<!--
**FueledByRage/FueledByRage** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
